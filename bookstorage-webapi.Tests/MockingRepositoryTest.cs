﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;

using bookstorage_webapi.Controllers;
using bookstorage_webapi.Models;
using bookstorage_webapi.Repositories;

using Moq;
using NUnit.Framework;
using Newtonsoft.Json;

namespace bookstorage_webapi.Tests
{
  [TestFixture]
  public class MockingRepositoryTest
    {
        Mock<IRepository<Book>> mockrepo;
        BooksController controller;

        [TestFixtureSetUp]
        public void Setup()
        {
            mockrepo = new Mock<IRepository<Book>>();
            IList<Book> list = new List<Book>();
            list.Add(new Book(2, "Hiba revut' voly", "Panas Myrnyi"));
            list.Add(new Book(3, "Natalka Poltavka", "Ivan Kotlyarevsky"));
            mockrepo.Setup(x => x.GetBooks()).Returns(list);
            mockrepo.Setup(x => x.GetBook(1)).Returns(new Book(1, "Tvory", "Lina Kostenko"));
            mockrepo.Setup(x => x.AddBook(new Book("Tvory", "Lina Kostenko"))).Returns(new Book(1, "Tvory", "Lina Kostenko"));
            mockrepo.Setup(x => x.UpdateBook(It.IsAny<Book>())).Returns(new Book(3, "Eneida", "Ivan Kotlyarevsky"));
            mockrepo.Setup(x => x.DeleteBook(4)).Returns(new Book(4, "Zemlya", "Olha Kobylyanska"));
            controller = new BooksController(mockrepo.Object);
            controller.Request = new HttpRequestMessage();
            var Configuration = new HttpConfiguration();
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = Configuration;
        }

        [Test]
        public void Mock_GetBooks()
        {
            var response = controller.GetBooks();
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var actual = response.Content.ReadAsAsync<IList<Book>>().Result;
            Assert.AreEqual("Panas Myrnyi", actual[0].Author);
        }


        [Test]
        public void Mock_GetBook()
        {
            var response = controller.GetBook(1);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            //   string json =response.Content.ReadAsStringAsync().Result;
            //   Book actual = JsonConvert.DeserializeObject<Book>(json);
            var actual = response.Content.ReadAsAsync<Book>().Result;
            Assert.AreEqual("Lina Kostenko", actual.Author);
        }

        [Test]
        public void Mock_AddBook()
        {
            var response = controller.PostBook(new Book("Tvory", "Lina Kostenko"));
            Assert.AreEqual(HttpStatusCode.Created,response.StatusCode);
        }

        [Test]
        public void Mock_UpdateBook()
        {
            var response = controller.PutBook(new Book(3, "Eneida", "Ivan Kotlyarevsky"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void Mock_DeleteBook()
        {
            var response = controller.DeleteBook(4);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
